Numero de clusters: 2

Amostras selecionadas como Raízes:
[1893  165]


Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.0014185905456542969)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.0009379386901855469)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.000990152359008789)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0005998611450195312)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.0016679763793945312)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.0010638236999511719)
Treino do RandomForest Terminado. (Tempo de execucao: 0.013090133666992188)
Treino do Neural Net Terminado. (Tempo de execucao: 0.22405219078063965)

Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.46
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.46
Matriz de Confusão:
[[267  20]
 [307   6]]
Precision: 0.34796
Recall: 0.47474
F1-score: 0.32780
(Tempo de execucao: 0.01950)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.60
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.64
Matriz de Confusão:
[[173 114]
 [103 210]]
Precision: 0.63748
Recall: 0.63686
F1-score: 0.63695
(Tempo de execucao: 0.01047)

AcuracGCHia obtida com o Decision Tree no Conjunto de Treinamento: 0.50
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.52
Matriz de Confusão:
[[160 127]
 [162 151]]
Precision: 0.52003
Recall: 0.51996
F1-score: 0.51822
(Tempo de execucao: 0.01246)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.51
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.48
Matriz de Confusão:
[[287   0]
 [313   0]]
Precision: 0.23917
Recall: 0.50000
F1-score: 0.32356
(Tempo de execucao: 0.15436)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.56
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.55
Matriz de Confusão:
[[252  35]
 [236  77]]
Precision: 0.60195
Recall: 0.56203
F1-score: 0.50634
(Tempo de execucao: 0.01489)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.51
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.48
Matriz de Confusão:
[[287   0]
 [313   0]]
Precision: 0.23917
Recall: 0.50000
F1-score: 0.32356
(Tempo de execucao: 0.01822)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.69
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.68
Matriz de Confusão:
[[257  30]
 [162 151]]
Precision: 0.72381
Recall: 0.68895
F1-score: 0.66969
(Tempo de execucao: 0.01819)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.60
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.65
Matriz de Confusão:
[[165 122]
 [ 88 225]]
Precision: 0.65029
Recall: 0.64688
F1-score: 0.64646
(Tempo de execucao: 0.02064)

Número de Amostras selecionadas Ativamente: 6

