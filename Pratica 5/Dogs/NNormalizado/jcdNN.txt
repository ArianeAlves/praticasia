
Numero de clusters: 2

Amostras selecionadas como Raízes:
[15955 13387]


Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.0014622211456298828)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.0013120174407958984)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.0010547637939453125)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0010035037994384766)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.0023255348205566406)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.0011065006256103516)
Treino do RandomForest Terminado. (Tempo de execucao: 0.013337135314941406)
Treino do Neural Net Terminado. (Tempo de execucao: 0.2267465591430664)

Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.50
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.49
Matriz de Confusão:
[[ 372 2143]
 [ 407 2078]]
Precision: 0.48492
Recall: 0.49206
F1-score: 0.42280
(Tempo de execucao: 0.11178)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.48
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.48
Matriz de Confusão:
[[ 610 1905]
 [ 697 1788]]
Precision: 0.47544
Recall: 0.48103
F1-score: 0.44902
(Tempo de execucao: 0.04356)

AcuracGCHia obtida com o Decision Tree no Conjunto de Treinamento: 0.46
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.47
Matriz de Confusão:
[[ 827 1688]
 [ 953 1532]]
Precision: 0.47019
Recall: 0.47266
F1-score: 0.46109
(Tempo de execucao: 0.05595)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.48
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.48
Matriz de Confusão:
[[ 251 2264]
 [ 344 2141]]
Precision: 0.45394
Recall: 0.48069
F1-score: 0.39145
(Tempo de execucao: 0.96291)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.46
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.46
Matriz de Confusão:
[[ 317 2198]
 [ 490 1995]]
Precision: 0.43430
Recall: 0.46443
F1-score: 0.39417
(Tempo de execucao: 0.04943)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.50
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.50
Matriz de Confusão:
[[   0 2515]
 [   0 2485]]
Precision: 0.24850
Recall: 0.50000
F1-score: 0.33200
(Tempo de execucao: 0.17306)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.47
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.47
Matriz de Confusão:
[[ 648 1867]
 [ 780 1705]]
Precision: 0.46555
Recall: 0.47189
F1-score: 0.44583
(Tempo de execucao: 0.06069)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.48
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.48
Matriz de Confusão:
[[ 608 1907]
 [ 691 1794]]
Precision: 0.47639
Recall: 0.48184
F1-score: 0.44942
(Tempo de execucao: 0.11744)

Número de Amostras selecionadas Ativamente: 14
