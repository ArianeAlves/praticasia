
 Média do Conjunto de Treinamento por Feature:
[ 9.36750677e-17 -1.45716772e-17 -3.13984949e-17 -5.56846236e-17
  7.63278329e-18  1.17961196e-17 -6.27969898e-17  1.41900380e-16
  7.56339436e-17 -2.39044895e-16  1.39471767e-16 -2.41820453e-16
  0.00000000e+00 -1.42247325e-16  1.29930788e-16 -1.27328703e-16
 -1.68615122e-16 -3.36883299e-16 -1.05124243e-16  0.00000000e+00
  9.24607613e-17 -1.28369537e-16  1.28369537e-16 -1.26461341e-16
 -2.49800181e-17  3.00107161e-17 -2.28983499e-17 -7.28583860e-18
  0.00000000e+00 -6.01949046e-17 -6.67868538e-17  1.06338549e-16
 -1.38777878e-18 -1.48839274e-16  8.39606162e-17  0.00000000e+00
  4.52762827e-16  1.04777298e-16 -4.92661467e-17  2.16840434e-17
  2.06432094e-16 -2.41820453e-16 -3.71924713e-16  3.46944695e-17
 -1.31318567e-16 -9.23740251e-17 -1.50573998e-16 -4.64905892e-17
  1.03042574e-16 -2.56739074e-16 -1.34614542e-16  1.36175793e-16
 -1.20042865e-16 -1.59594560e-17 -6.07153217e-17 -7.71951947e-17
  7.14706072e-17  1.92554306e-16 -2.19269047e-16 -7.28583860e-18
 -9.59302082e-17 -4.19803081e-17 -1.86656246e-16  6.86950496e-17
  2.71310752e-16  2.87964097e-16  1.82145965e-16  1.47451495e-17
  1.27675648e-16 -1.94289029e-16  1.83880688e-16  6.52256027e-17
  1.16226473e-16  6.73072709e-17  2.47371568e-16  5.81132364e-17
  0.00000000e+00  4.31946146e-17  2.93168267e-17  9.43689571e-17
 -3.21964677e-16 -9.08995101e-17  1.24900090e-17 -1.64972203e-16
 -2.12503626e-16  1.05471187e-16 -4.23272528e-17  1.91860416e-16
  2.38351006e-16 -1.53349555e-16 -1.76247905e-16  1.30798150e-16
 -1.99493200e-17  2.51534904e-17  7.70217223e-17  9.21138166e-17
 -1.07552856e-16 -5.44703171e-17 -6.07153217e-17 -3.13984949e-17
 -1.31145095e-16  1.88087393e-16 -7.63278329e-18  1.32185929e-16
  1.15879528e-16 -6.02122519e-16 -2.32452946e-17 -8.32667268e-17
  4.40619763e-17  4.87457297e-17  2.77555756e-16 -7.63278329e-17
 -2.42861287e-17 -1.04083409e-18 -4.23272528e-17  1.36175793e-16
  1.76941795e-16 -1.73472348e-17 -2.74086309e-17 -6.83481050e-17
 -3.50067197e-16  2.33840725e-16 -6.17995238e-18 -2.26901831e-16
 -8.32667268e-17  4.57966998e-17  5.13478149e-17 -1.38777878e-16]
Média do Conjunto de Treinamento por Feature:
[1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1. 1. 1. 1. 1. 1. 0. 1. 1. 1. 1.
 1. 1. 1. 1. 0. 1. 1. 1. 1. 1. 1. 0. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 0. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1.]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.0014674663543701172)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.006132602691650391)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.005488157272338867)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0014028549194335938)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.011819839477539062)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.007036447525024414)
Treino do RandomForest Terminado. (Tempo de execucao: 0.024153947830200195)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 0.37124204635620117)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.69
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.60
Matriz de Confusão:
[[ 7 15]
 [ 1 17]]
Precision: 0.70312
Recall: 0.63131
F1-score: 0.57333
(Tempo de execucao: 0.00583)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.82
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.70
Matriz de Confusão:
[[10 12]
 [ 0 18]]
Precision: 0.80000
Recall: 0.72727
F1-score: 0.68750
(Tempo de execucao: 0.00801)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 0.74
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.55
Matriz de Confusão:
[[12 10]
 [ 8 10]]
Precision: 0.55000
Recall: 0.55051F1-score: 0.54887
(Tempo de execucao: 0.00420)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.64
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.55
Matriz de Confusão:
[[ 5 17]
 [ 1 17]]
Precision: 0.66667
Recall: 0.58586
F1-score: 0.50549
(Tempo de execucao: 0.02891)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.46
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.38
Matriz de Confusão:
[[12 10]
 [15  3]]
Precision: 0.33761
Recall: 0.35606
F1-score: 0.34167
(Tempo de execucao: 0.00465)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.59
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.57
Matriz de Confusão:
[[ 6 16]
 [ 1 17]]
Precision: 0.68615
Recall: 0.60859
F1-score: 0.54023
(Tempo de execucao: 0.01367)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.81
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.82
Matriz de Confusão:
[[21  1]
 [ 6 12]]
Precision: 0.85043
Recall: 0.81061
F1-score: 0.81567
(Tempo de execucao: 0.00971)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.81
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.68
Matriz de Confusão:
[[ 9 13]
 [ 0 18]]
Precision: 0.79032
Recall: 0.70455
F1-score: 0.65767
(Tempo de execucao: 0.00583)
