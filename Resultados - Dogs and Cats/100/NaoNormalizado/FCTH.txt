
 Média do Conjunto de Treinamento por Feature:
[2.625   3.86875 3.69375 0.6625  0.7875  1.475   3.3625  3.2625  4.125
 1.60625 1.18125 1.84375 0.3625  0.2125  0.81875 0.36875 0.13125 0.38125
 0.9375  0.29375 0.98125 0.15    0.04375 0.21875 0.35    1.2875  0.35625
 0.1625  0.1375  0.2     1.      0.925   1.25625 0.2875  0.2     0.40625
 0.075   0.01875 0.13125 0.03125 0.0125  0.0375  0.1625  0.0375  0.175
 0.01875 0.      0.01875 0.1     0.7125  0.125   0.03125 0.04375 0.03125
 0.45625 0.41875 0.64375 0.0875  0.0875  0.15625 0.025   0.00625 0.04375
 0.      0.      0.      0.05    0.00625 0.04375 0.00625 0.      0.00625
 0.      0.33125 0.      0.0125  0.00625 0.00625 0.15    0.15    0.2375
 0.03125 0.01875 0.06875 0.00625 0.      0.0125  0.      0.      0.
 0.      0.      0.01875 0.      0.      0.      0.      0.13125 0.
 0.      0.      0.00625 0.01875 0.0125  0.025   0.      0.      0.01875
 0.00625 0.      0.      0.      0.      0.      0.      0.      0.
 0.      0.      0.      0.      0.1375  0.      0.00625 0.      0.
 0.01875 0.0125  0.03125 0.00625 0.00625 0.00625 0.00625 0.      0.
 0.      0.      0.      0.      0.      0.00625 0.      0.      0.
 0.      0.05    0.      0.      0.      0.      0.00625 0.      0.0125
 0.      0.      0.00625 0.      0.      0.      0.      0.      0.
 0.      0.      0.      0.      0.      0.      0.      0.0875  0.
 0.      0.      0.      0.0125  0.      0.01875 0.      0.      0.00625
 0.      0.      0.      0.      0.      0.      0.      0.      0.
 0.      0.      0.     ]
Média do Conjunto de Treinamento por Feature:
[2.00546129 2.3321714  2.13013167 1.2035754  1.38918816 1.67686463
 1.97321913 2.2816866  1.98037244 1.58152172 1.60028074 1.82533173
 0.89083879 0.77771701 1.27412654 0.97225174 0.62371743 0.90049899
 1.34018422 0.76319129 1.34848005 0.58309519 0.30304445 0.60901432
 0.52678269 1.07463657 0.55166651 0.44563859 0.46754011 0.43011626
 0.8291562  1.06389614 0.86780524 0.54068822 0.50990195 0.66423711
 0.30720514 0.13564084 0.37285847 0.17399264 0.11110243 0.2471715
 0.44563859 0.2204399  0.494343   0.13564084 0.         0.13564084
 0.32015621 0.90407066 0.33071891 0.17399264 0.20453835 0.17399264
 0.56840649 0.65642855 0.59526123 0.28256636 0.28256636 0.39602517
 0.15612495 0.0788095  0.20453835 0.         0.         0.
 0.21794495 0.0788095  0.2331007  0.0788095  0.         0.0788095
 0.         0.56703037 0.         0.11110243 0.0788095  0.0788095
 0.37416574 0.35707142 0.4399929  0.17399264 0.13564084 0.2766287
 0.0788095  0.         0.11110243 0.         0.         0.
 0.         0.         0.13564084 0.         0.         0.
 0.         0.33767357 0.         0.         0.         0.0788095
 0.13564084 0.11110243 0.15612495 0.         0.         0.13564084
 0.0788095  0.         0.         0.         0.         0.
 0.         0.         0.         0.         0.         0.
 0.         0.34437443 0.         0.0788095  0.         0.
 0.13564084 0.11110243 0.17399264 0.0788095  0.0788095  0.0788095
 0.0788095  0.         0.         0.         0.         0.
 0.         0.         0.0788095  0.         0.         0.
 0.         0.21794495 0.         0.         0.         0.
 0.0788095  0.         0.11110243 0.         0.         0.0788095
 0.         0.         0.         0.         0.         0.
 0.         0.         0.         0.         0.         0.
 0.         0.30388114 0.         0.         0.         0.
 0.11110243 0.         0.13564084 0.         0.         0.0788095
 0.         0.         0.         0.         0.         0.
 0.         0.         0.         0.         0.         0.        ]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.0013387203216552734)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.0025136470794677734)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.002229452133178711)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0011680126190185547)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.026140451431274414)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.01242828369140625)
Treino do RandomForest Terminado. (Tempo de execucao: 0.03470444679260254)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 0.5260396003723145)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.68
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.53
Matriz de Confusão:
[[ 5 17]
 [ 2 16]]
Precision: 0.59957
Recall: 0.55808
F1-score: 0.48614
(Tempo de execucao: 0.00830)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.90
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.80
Matriz de Confusão:
[[18  4]
 [ 4 14]]
Precision: 0.79798
Recall: 0.79798
F1-score: 0.79798
(Tempo de execucao: 0.00800)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 1.00
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.68
Matriz de Confusão:
[[12 10]
 [ 3 15]]
Precision: 0.70000
Recall: 0.68939F1-score: 0.67316
(Tempo de execucao: 0.00671)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.88
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.78
Matriz de Confusão:
[[17  5]
 [ 4 14]]
Precision: 0.77318
Recall: 0.77525
F1-score: 0.77373
(Tempo de execucao: 0.05825)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.96
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.65
Matriz de Confusão:
[[13  9]
 [ 5 13]]
Precision: 0.65657
Recall: 0.65657
F1-score: 0.65000
(Tempo de execucao: 0.00643)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.88
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.72
Matriz de Confusão:
[[17  5]
 [ 6 12]]
Precision: 0.72251
Recall: 0.71970
F1-score: 0.72063
(Tempo de execucao: 0.02085)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.97
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.75
Matriz de Confusão:
[[17  5]
 [ 5 13]]
Precision: 0.74747
Recall: 0.74747
F1-score: 0.74747
(Tempo de execucao: 0.01425)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 1.00
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.75
Matriz de Confusão:
[[16  6]
 [ 4 14]]
Precision: 0.75000
Recall: 0.75253
F1-score: 0.74937
(Tempo de execucao: 0.00797)

