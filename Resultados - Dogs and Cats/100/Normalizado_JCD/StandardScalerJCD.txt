
 Média do Conjunto de Treinamento por Feature:
[ 1.99840144e-16 -1.06165077e-16 -1.69309011e-16 -1.35308431e-16
  4.51028104e-17 -2.77555756e-18 -3.78863607e-16 -1.77635684e-16
 -5.82867088e-17  1.04430353e-15  5.27355937e-17 -4.64212002e-16
  2.42861287e-17 -1.20736754e-16 -9.99200722e-17 -2.42861287e-17
 -1.36349265e-16  1.58900670e-16  5.12003634e-16  7.77156117e-17
  7.25808302e-16  1.66533454e-17  1.45716772e-17  1.38777878e-16
  1.09634524e-16 -2.78943535e-16 -1.33226763e-16 -8.60422844e-17
 -4.82253126e-17 -1.36696210e-16  3.80251386e-16  3.43735457e-16
 -3.61429636e-16  2.49800181e-17  1.59594560e-17  1.76247905e-16
 -1.11716192e-16 -1.94635974e-16  1.31145095e-16 -5.03069808e-17
 -7.58074159e-17 -4.78783679e-17  1.16573418e-16  0.00000000e+00
  2.35922393e-16 -1.58206781e-16  0.00000000e+00  9.64506253e-17
 -4.99600361e-17 -1.11022302e-17 -5.55111512e-18 -2.02962647e-16
 -3.34801631e-17  1.70696790e-16 -6.93889390e-18 -1.19348975e-16
  3.08455518e-17  2.28983499e-17  6.10622664e-17 -7.63278329e-18
 -1.46063717e-16  1.90472638e-16 -5.48172618e-17  1.14491749e-16
 -1.58206781e-16  7.97972799e-18 -1.86309301e-16  1.48492330e-16
  8.04911693e-17  0.00000000e+00  0.00000000e+00 -8.41340886e-17
  1.13728471e-15 -2.00811590e-15 -5.55111512e-17  1.33226763e-16
  3.85108612e-17  1.99840144e-16 -8.07687250e-16 -2.77555756e-17
 -1.98036032e-15 -9.78384040e-17 -8.04911693e-17 -4.02455846e-17
 -1.72084569e-16  1.03042574e-16  1.96370697e-16  1.19695920e-16
 -1.58206781e-16 -1.31838984e-16  1.52655666e-16  7.77156117e-17
  4.31599201e-16  1.05471187e-16 -2.93168267e-17  1.92207361e-16
  1.24900090e-16 -1.23512311e-16  6.31439345e-16  7.04297731e-17
  1.15185639e-16  2.41473508e-16 -7.63278329e-17 -2.47024623e-16
 -8.13151629e-17 -5.89805982e-17  2.63677968e-17 -4.78783679e-17
 -2.39391840e-17  1.90472638e-16 -1.14491749e-16  8.04911693e-17
  0.00000000e+00 -2.25514052e-17 -5.55111512e-18 -7.80625564e-18
 -9.57567359e-17  0.00000000e+00  0.00000000e+00  8.15320034e-17
 -1.58206781e-16  3.83026943e-16  0.00000000e+00  1.21257171e-16
  0.00000000e+00 -1.52829138e-16 -7.56339436e-17 -8.06646416e-17
  6.17561557e-17  4.51028104e-18 -1.52829138e-16  2.39391840e-17
 -8.36136715e-17 -2.00360561e-16  6.21031004e-17  0.00000000e+00
  0.00000000e+00  0.00000000e+00  0.00000000e+00  0.00000000e+00
  0.00000000e+00  0.00000000e+00  0.00000000e+00  0.00000000e+00
  2.08860707e-16 -7.41767758e-16 -5.82867088e-17 -1.72084569e-16
  6.14092110e-17  1.72778458e-16  1.53089347e-17  1.94289029e-16
  5.25621213e-17  7.63278329e-18 -9.74914593e-17 -1.16573418e-16
 -1.41900380e-16 -1.76594850e-16  8.95117314e-17  8.04911693e-17
 -1.58206781e-16  3.85108612e-17  6.73072709e-17  1.11369247e-16
 -9.85322934e-17  0.00000000e+00  0.00000000e+00  1.11022302e-16]
Média do Conjunto de Treinamento por Feature:
[1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 0. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1. 1. 1. 1. 0. 0. 1.
 1. 1. 0. 1. 0. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 0. 1.]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.0020132064819335938)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.015084266662597656)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.005674600601196289)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0015988349914550781)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.03631734848022461)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.019534587860107422)
Treino do RandomForest Terminado. (Tempo de execucao: 0.058125972747802734)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 0.5096704959869385)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.54
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.45
Matriz de Confusão:
[[ 0 22]
 [ 0 18]]
Precision: 0.22500
Recall: 0.50000
F1-score: 0.31034
(Tempo de execucao: 0.00675)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.81
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.57
Matriz de Confusão:
[[16  6]
 [11  7]]
Precision: 0.56553
Recall: 0.55808
F1-score: 0.55234
(Tempo de execucao: 0.00711)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 0.75
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.38
Matriz de Confusão:
[[11 11]
 [14  4]]
Precision: 0.35333
Recall: 0.36111F1-score: 0.35525
(Tempo de execucao: 0.00678)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.73
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.57
Matriz de Confusão:
[[16  6]
 [11  7]]
Precision: 0.56553
Recall: 0.55808
F1-score: 0.55234
(Tempo de execucao: 0.05566)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.69
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.68
Matriz de Confusão:
[[14  8]
 [ 5 13]]
Precision: 0.67794
Recall: 0.67929
F1-score: 0.67480
(Tempo de execucao: 0.01052)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.70
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.80
Matriz de Confusão:
[[17  5]
 [ 3 15]]
Precision: 0.80000
Recall: 0.80303
F1-score: 0.79950
(Tempo de execucao: 0.03055)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.85
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.68
Matriz de Confusão:
[[18  4]
 [ 9  9]]
Precision: 0.67949
Recall: 0.65909
F1-score: 0.65767
(Tempo de execucao: 0.02086)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.95
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.62
Matriz de Confusão:
[[17  5]
 [10  8]]
Precision: 0.62251
Recall: 0.60859
F1-score: 0.60500
(Tempo de execucao: 0.01160)
