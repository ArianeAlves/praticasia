
 Média do Conjunto de Treinamento por Feature:
[ 3.33960724e-17 -1.25310873e-16  2.29309315e-15 -8.52704782e-15
  8.38387693e-16 -1.72373834e-17 -2.90726818e-16 -9.04229989e-16
  2.23090869e-15 -6.86457835e-16 -3.54760925e-17 -1.07661796e-16
 -1.94897206e-15 -2.33132645e-15 -1.09145609e-15]
Média do Conjunto de Treinamento por Feature:
[0.03022083 0.06621403 0.18144046 0.10168137 0.17597476 0.029851
 0.066143   0.18401986 0.10203984 0.16316423 0.02956062 0.06798729
 0.17869963 0.10092789 0.15871289]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.024341344833374023)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.13460350036621094)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.6387555599212646)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.022028446197509766)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.04783129692077637)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 25.138603448867798)
Treino do RandomForest Terminado. (Tempo de execucao: 0.8791346549987793)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 2.3518075942993164)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.50
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.50
Matriz de Confusão:
[[2515    0]
 [2484    1]]
Precision: 0.75155
Recall: 0.50020
F1-score: 0.33511
(Tempo de execucao: 0.03745)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.60
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.59
Matriz de Confusão:
[[1424 1091]
 [ 952 1533]]
Precision: 0.59177
Recall: 0.59155
F1-score: 0.59121
(Tempo de execucao: 0.05782)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 0.51
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.52
Matriz de Confusão:
[[1961  554]
 [1845  640]]
Precision: 0.52563
Recall: 0.51863F1-score: 0.48420
(Tempo de execucao: 0.06395)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.50
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.52
Matriz de Confusão:
[[1738  777]
 [1630  855]]
Precision: 0.51997
Recall: 0.51756
F1-score: 0.50310
(Tempo de execucao: 6.11675)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.60
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.59
Matriz de Confusão:
[[1391 1124]
 [ 911 1574]]
Precision: 0.59383
Recall: 0.59324
F1-score: 0.59245
(Tempo de execucao: 0.05771)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.61
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.60
Matriz de Confusão:
[[1957  558]
 [1419 1066]]
Precision: 0.61804
Recall: 0.60355
F1-score: 0.59163
(Tempo de execucao: 23.17317)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.52
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.52
Matriz de Confusão:
[[2201  314]
 [2078  407]]
Precision: 0.53943
Recall: 0.51947
F1-score: 0.45091
(Tempo de execucao: 0.07559)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.58
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.58
Matriz de Confusão:
[[2361  154]
 [1938  547]]
Precision: 0.66476
Recall: 0.57944
F1-score: 0.51818
(Tempo de execucao: 0.08943)
