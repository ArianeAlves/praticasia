
 Média do Conjunto de Treinamento por Feature:
[3.63189299e-03 1.80127936e+02 2.90060240e-01 8.24229942e+00
 1.99790684e+04 3.57666385e-03 1.80239423e+02 2.92395016e-01
 8.22177916e+00 1.77234545e+04 3.53996941e-03 1.79008634e+02
 2.86735237e-01 8.22465042e+00 1.56541113e+04]
Média do Conjunto de Treinamento por Feature:
[2.40843397e-02 2.38532715e+02 1.15019862e-01 7.23208930e-01
 7.77077370e+03 2.38180624e-02 2.40143468e+02 1.16322211e-01
 7.25316025e-01 7.15686539e+03 2.35487782e-02 2.34895098e+02
 1.13856071e-01 7.14984699e-01 7.27295877e+03]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.012849569320678711)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.5842645168304443)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.6634113788604736)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.022235870361328125)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.05078387260437012)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 42.4181342124939)
Treino do RandomForest Terminado. (Tempo de execucao: 0.8820393085479736)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 25.985921144485474)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.57
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.57
Matriz de Confusão:
[[2199  316]
 [1818  667]]
Precision: 0.61298
Recall: 0.57138
F1-score: 0.52898
(Tempo de execucao: 0.11374)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.60
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.59
Matriz de Confusão:
[[1549  966]
 [1100 1385]]
Precision: 0.58693
Recall: 0.58662
F1-score: 0.58635
(Tempo de execucao: 0.06465)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 1.00
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.55
Matriz de Confusão:
[[1395 1120]
 [1128 1357]]
Precision: 0.55038
Recall: 0.55037F1-score: 0.55037
(Tempo de execucao: 0.07179)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.71
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.53
Matriz de Confusão:
[[1394 1121]
 [1210 1275]]
Precision: 0.53373
Recall: 0.53368
F1-score: 0.53354
(Tempo de execucao: 1.68248)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.61
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.60
Matriz de Confusão:
[[1523  992]
 [1017 1468]]
Precision: 0.59818
Recall: 0.59816
F1-score: 0.59815
(Tempo de execucao: 0.05856)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 1.00
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.50
Matriz de Confusão:
[[   1 2514]
 [   0 2485]]
Precision: 0.74855
Recall: 0.50020
F1-score: 0.33244
(Tempo de execucao: 22.05498)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.98
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.58
Matriz de Confusão:
[[1686  829]
 [1272 1213]]
Precision: 0.58200
Recall: 0.57925
F1-score: 0.57601
(Tempo de execucao: 0.10604)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.57
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.56
Matriz de Confusão:
[[2254  261]
 [1932  553]]
Precision: 0.60891
Recall: 0.55938
F1-score: 0.50399
(Tempo de execucao: 0.09032)
