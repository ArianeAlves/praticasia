
 Média do Conjunto de Treinamento por Feature:
[ 5.75234305e-16 -4.47558657e-17 -5.14865928e-16 -1.62370117e-16
  3.51108032e-16 -2.79637424e-16 -3.08086889e-16  1.25593980e-16
 -3.13638004e-16  3.56659147e-16  6.18255447e-16 -5.05845366e-16
 -9.50628465e-17 -1.25247035e-16 -1.02695630e-16  5.55111512e-17
  1.06858966e-16 -7.00828284e-17  1.47104551e-16  1.14491749e-16
 -6.80011603e-17 -7.87564458e-17 -1.11369247e-16 -3.43475248e-16
  9.71445147e-18 -8.34055047e-16 -6.24500451e-16  7.28583860e-17
  1.26981758e-16  1.88737914e-16 -8.82627305e-16 -1.56125113e-16
  5.55111512e-16  3.14331894e-16  1.81105131e-16  7.18175519e-16
 -4.37150316e-17 -1.63064007e-17  2.99760217e-16 -9.15933995e-17
 -7.77156117e-17  9.81853487e-17  1.76247905e-16  1.41553436e-16
  9.08995101e-17  2.46677678e-16 -2.27769192e-16  1.39124823e-16
  5.13478149e-17  1.12687637e-15 -8.74300632e-17 -1.04083409e-17
 -6.10622664e-17 -9.92261828e-17 -5.68989300e-17  2.54050253e-16
  2.98372438e-17 -2.08166817e-18 -7.00828284e-17 -7.97972799e-17
  4.99600361e-17 -6.69603262e-17  1.31145095e-16  4.47558657e-17
 -5.72458747e-18  5.58580959e-17  2.63677968e-16  2.86229374e-16
 -6.45317133e-17  5.36029554e-17  0.00000000e+00  3.99680289e-16
  4.84334794e-16 -6.49480469e-16 -3.60822483e-17  8.18789481e-17
  8.67361738e-17  6.66133815e-17 -4.13558077e-16 -6.05071548e-16
 -9.73526815e-16  5.01682029e-16  1.73472348e-17  3.29597460e-16
 -1.66533454e-17 -5.55111512e-18  1.57512892e-16  3.46944695e-18
  1.52655666e-17  4.78783679e-17  6.45317133e-17 -1.80411242e-16
  9.57567359e-17 -2.11636264e-17 -5.41233725e-17 -1.09981468e-16
  2.77555756e-17  1.17683641e-15 -1.40165657e-16  8.25728375e-17
  3.05311332e-17  9.22872889e-17 -5.55111512e-17  1.63888000e-16
  5.75928194e-17  5.30825384e-17 -1.43982049e-16 -2.70616862e-16
 -1.78676518e-16 -5.72458747e-18 -1.63064007e-16  1.22471477e-16
 -1.21430643e-17 -1.84921523e-16 -4.64905892e-17 -3.01321468e-16
  2.77555756e-18  0.00000000e+00  0.00000000e+00  3.99680289e-16
  0.00000000e+00  2.76167977e-16  0.00000000e+00  1.19348975e-16
 -1.89431804e-16 -1.89431804e-16  9.61036806e-17  6.10622664e-17
 -1.24900090e-16  1.16920362e-16 -5.86336535e-17 -1.08767162e-16
 -1.64972203e-16  0.00000000e+00 -1.28196065e-16  0.00000000e+00
  0.00000000e+00  0.00000000e+00  0.00000000e+00  0.00000000e+00
 -3.01841885e-17  0.00000000e+00  0.00000000e+00  0.00000000e+00
  3.13638004e-16  6.41153797e-16  7.04991621e-16  4.92661467e-17
  5.62050406e-17 -1.31838984e-16 -1.06858966e-16  8.05648950e-16
 -6.66133815e-16  3.53883589e-17 -1.20736754e-16  1.30451205e-16
  3.01841885e-17  1.70696790e-16  5.62050406e-17 -3.50414142e-17
 -6.14092110e-17 -3.43475248e-17 -1.51961776e-16 -3.00107161e-16
 -1.22818422e-16  8.63892291e-17  0.00000000e+00  7.32053307e-17]
Média do Conjunto de Treinamento por Feature:
[1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 0. 1.
 0. 1. 0. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1. 0. 0. 0. 0. 0. 1. 0. 0. 0.
 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 0. 1.]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.006822824478149414)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.012802839279174805)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.0042531490325927734)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0012748241424560547)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.015770912170410156)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.008545637130737305)
Treino do RandomForest Terminado. (Tempo de execucao: 0.023373126983642578)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 0.4046294689178467)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.50
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.53
Matriz de Confusão:
[[21  1]
 [18  0]]
Precision: 0.26923
Recall: 0.47727
F1-score: 0.34426
(Tempo de execucao: 0.00720)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.96
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.75
Matriz de Confusão:
[[18  4]
 [ 6 12]]
Precision: 0.75000
Recall: 0.74242
F1-score: 0.74425
(Tempo de execucao: 0.00961)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 0.59
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.55
Matriz de Confusão:
[[11 11]
 [ 7 11]]
Precision: 0.55556
Recall: 0.55556F1-score: 0.55000
(Tempo de execucao: 0.00457)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 1.00
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.60
Matriz de Confusão:
[[10 12]
 [ 4 14]]
Precision: 0.62637
Recall: 0.61616
F1-score: 0.59596
(Tempo de execucao: 0.03317)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.57
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.53
Matriz de Confusão:
[[13  9]
 [10  8]]
Precision: 0.51790
Recall: 0.51768
F1-score: 0.51746
(Tempo de execucao: 0.00491)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.88
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.82
Matriz de Confusão:
[[20  2]
 [ 5 13]]
Precision: 0.83333
Recall: 0.81566
F1-score: 0.81947
(Tempo de execucao: 0.01377)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.93
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.78
Matriz de Confusão:
[[17  5]
 [ 4 14]]
Precision: 0.77318
Recall: 0.77525
F1-score: 0.77373
(Tempo de execucao: 0.00994)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.99
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.82
Matriz de Confusão:
[[18  4]
 [ 3 15]]
Precision: 0.82331
Recall: 0.82576
F1-score: 0.82401
(Tempo de execucao: 0.00596)
