
 Média do Conjunto de Treinamento por Feature:
[ 1.79844565e-15  1.72873001e-15  4.23036606e-15  3.77578755e-15
 -5.52433533e-17 -6.18613956e-16]
Média do Conjunto de Treinamento por Feature:
[0.26322282 0.23630916 0.41241491 0.2869178  0.04842639 0.13204538]
Treino do Gaussian Naive Bayes Terminado. (Tempo de execucao: 0.003370046615600586)
Treino do Logistic Regression Terminado. (Tempo de execucao: 0.026988983154296875)
Treino do Decision Tree Terminado. (Tempo de execucao: 0.08930373191833496)
Treino do K-Nearest Neighbors Terminado. (Tempo de execucao: 0.0023801326751708984)
Treino do Linear Discriminant Analysis Terminado. (Tempo de execucao: 0.005418300628662109)
Treino do Support Vector Machine Terminado. (Tempo de execucao: 0.37298059463500977)
Treino do RandomForest Terminado. (Tempo de execucao: 0.0761423110961914)
TreinoDesvio Padrão do Conjunto de Treinamento por Feature: do Neural Net Terminado. (Tempo de execucao: 3.083341360092163)
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Treinamento: 0.50
Acuracia obtida com o Gaussian Naive Bayes no Conjunto de Teste: 0.53
Matriz de Confusão:
[[  5 282]
 [  0 313]]
Precision: 0.76303
Recall: 0.50871
F1-score: 0.36184
(Tempo de execucao: 0.01185)

Acuracia obtida com o Logistic Regression no Conjunto de Treinamento: 0.60
Acuracia obtida com o Logistic Regression no Conjunto de Teste: 0.56
Matriz de Confusão:
[[187 100]
 [161 152]]
Precision: 0.57027
Recall: 0.56860
F1-score: 0.56351
(Tempo de execucao: 0.00896)

Acuracia obtida com o Decision Tree no Conjunto de Treinamento: 0.54
Acuracia obtida com o Decision Tree no Conjunto de Teste: 0.54
Matriz de Confusão:
[[112 175]
 [102 211]]
Precision: 0.53500
Recall: 0.53218F1-score: 0.52541
(Tempo de execucao: 0.01432)

Acuracia obtida com o K-Nearest Neighbors no Conjunto de Treinamento: 0.52
Acuracia obtida com o K-Nearest Neighbors no Conjunto de Teste: 0.49
Matriz de Confusão:
[[184 103]
 [203 110]]
Precision: 0.49594
Recall: 0.49628
F1-score: 0.48212
(Tempo de execucao: 0.25858)

Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Treinamento: 0.60
Acuracia obtida com o Linear Discriminant Analysis no Conjunto de Teste: 0.56
Matriz de Confusão:
[[184 103]
 [161 152]]
Precision: 0.56471
Recall: 0.56337
F1-score: 0.55874
(Tempo de execucao: 0.00864)

Acuracia obtida com o Support Vector Machine no Conjunto de Treinamento: 0.59
Acuracia obtida com o Support Vector Machine no Conjunto de Teste: 0.55
Matriz de Confusão:
[[177 110]
 [160 153]]
Precision: 0.55349
Recall: 0.55277
F1-score: 0.54928
(Tempo de execucao: 0.32734)

Acuracia obtida com o RandomForest no Conjunto de Treinamento: 0.52
Acuracia obtida com o RandomForest no Conjunto de Teste: 0.49
Matriz de Confusão:
[[229  58]
 [247  66]]
Precision: 0.50668
Recall: 0.50439
F1-score: 0.45116
(Tempo de execucao: 0.01903)

Acuracia obtida com o Neural Net no Conjunto de Treinamento: 0.59
Acuracia obtida com o Neural Net no Conjunto de Teste: 0.55
Matriz de Confusão:
[[179 108]
 [161 152]]
Precision: 0.55554
Recall: 0.55466
F1-score: 0.55076
(Tempo de execucao: 0.02026)
