# -*- coding: utf-8 -*-
"""Prática Aprendizado Não-Supervisionado.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/15ZCJ3a6ndfn7PiW0JBfxvk8iK2DbLum6

# Prática de Aprendizado Não-Supervisionado

**Importando bibliotecas e funções**
"""

import pandas as pd
import numpy as np
from math import sqrt
from subprocess import call
import os
import json
import plotly
import plotly.offline as py
import plotly.graph_objs as go
from sklearn.cluster import KMeans
from sklearn import preprocessing
from scipy.io.arff import loadarff 
from sklearn.decomposition import PCA
import warnings
from sklearn.metrics import silhouette_samples, silhouette_score
warnings.filterwarnings("ignore")

py.init_notebook_mode(connected=False)

def optimal_number_of_clusters(wcss):
    x1, y1 = 2, wcss[0]
    x2, y2 = 11, wcss[len(wcss)-1]

    distances = []
    for i in range(len(wcss)):
        x0 = i+2
        y0 = wcss[i]
        numerator = abs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)
        denominator = sqrt((y2 - y1)**2 + (x2 - x1)**2)
        distances.append(numerator/denominator)
    
    return distances.index(max(distances)) + 2

# Função para chamada do gráfico interativo

def configure_plotly_browser_state():
  import IPython
  display(IPython.core.display.HTML('''
        <script src="/static/components/requirejs/require.js"></script>
        <script>
          requirejs.config({
            paths: {
              base: '/static/base',
              plotly: 'https://cdn.plot.ly/plotly-1.43.1.min.js?noext',
            },
          });
        </script>
        '''))
#/home/ariane/Documents/IA/praticasia/Pratica 3/conclusoes/Dogs&Cats/JCD.arff
#/home/ariane/Documents/IA/praticasia/Pratica 3/conclusoes/Foods/JCD.arff
"""**Lendo o arquivo**"""
#tipoExtrato = ["BIC", 'FCTH','JCD', 'HaralickColor', 'MPO']
tipoExtrato = ['JCD']
titulo = "Dogs and Cats"
for count in range(len(tipoExtrato)):
    caminho = "/home/ariane/Documents/IA/praticasia/Pratica 3/conclusoes/Dogs&Cats/NNormalizadoCotovelo_"+tipoExtrato[count]
    
    if not os.path.exists(caminho):
        os.mkdir(caminho)
    # Carrega o .arff
    raw_data = loadarff("/home/ariane/Documents/IA/praticasia/Pratica 3/conclusoes/Dogs&Cats/"+tipoExtrato[count]+'.arff')
    # Transforma o .arff em um Pandas Dataframe
    df = pd.DataFrame(raw_data[0])
    # Imprime o Dataframe com suas colunas
    df.head()

    """**Retirando a classe do Dataset**"""

    # Com o iloc voce retira as linhas e colunas que quiser do Dataframe, no caso aqui sem as classes
    data_aux = df.iloc[:, 0:-1].values
    print(data_aux)


    with open(caminho+'/'+tipoExtrato[count]+'.txt','a') as file:
        data_normalized = data_aux
        file.write("\n#######Dados#############\n")
        file.write("\n"+str(data_normalized))
        file.close()

    """**Descobrindo o valor ideal de K**"""

    # Lista para armazenar os valores dados pelo método
    elbow = []
    # Numpy Array de valores para variar de 2 a 11 para servir de X no gráfico
    clusters = np.arange(2, 11)



    # Loop variando de 2 a 11 clusteres
    for i in range(2, 11):
      # Inicializando KMeans com I Clusters
        kmeans = KMeans(n_clusters = i, init = 'random')
      # Agrupa os dados
        kmeans.fit(data_normalized)
        with open(caminho+'/'+tipoExtrato[count]+'.txt','a') as file:
          # Imprime na tela
            file.write('\nElbow Method: Número de Cluster - {} Valor - {} '.format(i, kmeans.inertia_))
          # Armazena na lista os valores dado pelo método
            elbow.append(kmeans.inertia_)
            file.write('\n')
            file.close()

    n = optimal_number_of_clusters(elbow)
    # Plotando o gráfico de pontos + linha
    dados = go.Scatter(
        # Eixo x recebe o array de clusters
        x = clusters,
        # Eixo y recebe os valores do método
        y = elbow,
        # Define que o gráfico será de pontos e linhas
        mode = 'lines+markers',
        # Customina os marcadores
        marker = dict(
            # Define a cor
            color = '#36bce2',
            # Tamanho dos pontos
            size = 10,
            # Tamanho da linha ou contorno
            line = dict(width = 3)
        ),
    )

    # Alterando configurações de Layout do Gráfico
    layout = go.Layout(
        # Define Título
        title = 'Método Elbow',
        # Define o nome do eixo X
        xaxis = {'title': 'Número de Clusters'},
        # Define o nome do eixo Y
        yaxis = {'title':'Soma dos quadrados'},
        # Define a cor da borda e contorno do gráfico
        paper_bgcolor='rgba(245, 246, 249, 1)',
        # Define a cor do fundo do gráfico
        plot_bgcolor='rgba(245, 246, 249, 1)'
    )

    # Plotando
    data = [dados]
    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig)
    fig.write_image(caminho+'/'+'{}_01.png'.format(tipoExtrato[count]))


    # call(['orca', 'graph', json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder), 
    # '-o',caminho+'/'+'{}_{}_01.png'.format(tipoExtrato[count],nomeArq)])
    """**Clusterização**"""

    #Aplicando PCA para descobrir as 2 melhores features
    pca = PCA(n_components = 2)
    bestFeatures = pca.fit_transform(data_normalized)
    with open(caminho+'/'+tipoExtrato[count]+'.txt','a') as file:
        # Quando diminuimos diversas dimensões em apenas duas, perdemos muitas informações;
        # Com esse atributo podemos ver qual a porcentagem de informação foi perdida.
        file.write('\nVARIÂNCIA DAS MELHORES FEATURES')
        file.write('\n'+str(pca.explained_variance_ratio_))
        file.write('\n')

        # ALGUNS PARÂMETROS DO MÉTODO KMEANS

        # A variável n_clusters determina o número de agrupamento que queremos gerar com os dados;
        # Esse número normalmente é o número de classes, mas devido a complexidade de alguns conjuntos de dados talvez ele não seja o ideal;
        # Existem técnicas para descobrir o valor K ideal para o dataset, assim como fizemos acima.

        # O parametro init se refere ao modo como o algoritmo será inicializado.
        # A lib Scikit-Learn nos fornece três métodos de inicialização, sendo eles:
        # k-means++: É o método padrão, e os centróides serão gerados utilizando um método inteligente que favorece a convergência.
        # random: Inicializa de forma aleatória, ou seja, os centróides iniciais serão gerados de forma totalmente aleatória sem um critério para seleção.
        # ndarray: Esse podemos especificar um array de valores indicando qual seriam os centróides que o algoritmo deveria utilizar para a inicialização.

        # O parâmetro max_iter se refere a quantidade máxima de vezes que o algoritmo irá executar, por padrão o valor é 300 iterações.
        # elbow.index(max(elbow))+2
        # Inicializa o KMeans com 3 clusters e modo de inicialização aleatória.
        kmeans = KMeans(n_clusters = n, init = 'random')

        # Aqui de fato agrupamos os dados que queremos.
        kmeans.fit(bestFeatures)
        file.write("\nNumero de Clusters pela técnicas do cotovelo: "+ str(n))
        # Nesta variável já temos as coordenadas dos centróides gerados pelo agrupamento.
        file.write('\nCOORDENADA DOS CENTROIDES')
        file.write('\n')
        file.write('\n'+str(kmeans.cluster_centers_))
        file.write('\n')

        # Uma funcionalidade interessante é a função fit_transform;
        # Ela executa o K-means para agrupar os dados e retorna uma tabela de distâncias;
        # É gerada de forma que em cada instância contém os valores de distância em relação a cada cluster.
        distance = kmeans.fit_transform(bestFeatures)
        file.write('\nTABELA DE DISTÂNCIAS')
        file.write('\n')
        file.write('\n'+str(distance))
        file.write('\n')

        # O atributo labels_ nos retorna os labels para cada instância, ou seja, o número do cluster que a instância de dados foi atribuída.
        labels = kmeans.labels_

        # Já o predict serve para testar novas amostras e ver a qual clusters elas são atribuidas
        preds = kmeans.predict(bestFeatures)
        silhouette_avg = silhouette_score(bestFeatures, labels, metric='sqeuclidean')
        file.write('\nRÓTULO DADO PELO KMEANS DAS AMOSTRAS: Silhueta '+ str(silhouette_avg))
        file.write('\n')
        file.write('\n'+str(labels))
        file.write('\n'+str(preds))
        file.write('\n')
        file.close()

    """**Visualizando o Agrupamento**"""

    # Plotando o gráfico das Amostras
    dados1 = go.Scatter(
        # COM PCA
        # Eixo x 
        x = bestFeatures[:, 0],
        # Eixo y 
        y = bestFeatures[:, 1],
        # SEM PCA
        # Eixo x 
        #x = data_normalized[:, 0],
        # Eixo y 
        #y = data_normalized[:, 1],
        # Define que o gráfico será de pontos
        mode = 'markers',
        # Define o nome que será impresso na legenda do gráfico
        name = 'Instâncias',
        # Customina os marcadores
        marker = dict(
            # Define a cor
            color = preds,
            # Define a paleta de cores
            colorscale = 'Portland',
            # Tamanho dos pontos
            size = 10,
            # Opacidade dos pontos
            opacity = 1.0,
            # Tamanho da linha ou contorno
            line = dict(width = 1)
        ),
        showlegend = False
    )

    # Plotando o gráfico dos Centróides
    dados2 = go.Scatter(
        # Eixo x 
        x = kmeans.cluster_centers_[:, 0],
        # Eixo y 
        y = kmeans.cluster_centers_[:, 1],
        # Define que o gráfico será de pontos
        mode = 'markers',
        # Define o nome que será impresso na legenda do gráfico
        name = 'Clusters',
        # Customina os marcadores
        marker = dict(
            # Define a cor
            color = 'black',
            # Tamanho dos pontos
            size = 20,
            # Opacidade dos pontos
            opacity = 1.0,
            # Tamanho da linha ou contorno
            line = dict(width = 2)
        ),
    )

    # Alterando configurações de Layout do Gráfico
    layout = go.Layout(
        # Define Título
        title = 'Clusterização',
        # Define o nome do eixo X
        #xaxis = {'title': 'Número de Clusters'},
        # Define o nome do eixo Y
        #yaxis = {'title':'Soma dos quadrados'},
        # Define a cor da borda e contorno do gráfico
        paper_bgcolor='rgba(245, 246, 249, 1)',
        # Define a cor do fundo do gráfico
        plot_bgcolor='rgba(245, 246, 249, 1)'
    )

    # Plotando
    data = [dados1, dados2]
    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig)
    fig.write_image(caminho+'/'+'{}_02.png'.format(tipoExtrato[count]))
    

    """**Resetar as variáveis**"""

    # Commented out IPython magic to ensure Python compatibility.
    # %reset