
#######Dados#############

[[5. 4. 6. ... 0. 0. 0.]
 [4. 7. 1. ... 0. 0. 0.]
 [1. 6. 2. ... 0. 0. 0.]
 ...
 [5. 6. 1. ... 0. 0. 0.]
 [0. 1. 2. ... 0. 0. 0.]
 [3. 3. 5. ... 0. 0. 0.]]
Elbow Method: Número de Cluster - 2 Valor - 172204.42308909105

Elbow Method: Número de Cluster - 3 Valor - 160788.84661770143

Elbow Method: Número de Cluster - 4 Valor - 150853.3681876391

Elbow Method: Número de Cluster - 5 Valor - 143459.2203234286

Elbow Method: Número de Cluster - 6 Valor - 137028.112377491

Elbow Method: Número de Cluster - 7 Valor - 131773.14917049496

Elbow Method: Número de Cluster - 8 Valor - 127253.90254796426

Elbow Method: Número de Cluster - 9 Valor - 124187.83654562985

Elbow Method: Número de Cluster - 10 Valor - 121418.99871100836

VARIÂNCIA DAS MELHORES FEATURES
[0.21599745 0.11807679]

COORDENADA DOS CENTROIDES

[[-3.38481365 -0.80217874]
 [ 3.62280965 -1.3477001 ]
 [ 1.03828214  3.72701459]]

TABELA DE DISTÂNCIAS

[[ 2.94009649  5.06739165  3.51040417]
 [ 8.04961204  2.44686856  8.0665567 ]
 [ 3.91815584  5.44052001  8.03556145]
 ...
 [ 4.07692335  3.18106887  3.94924518]
 [ 6.25285928 10.99252246  6.46007926]
 [ 3.51759135  8.64156755  5.21544742]]

RÓTULO DADO PELO KMEANS DAS AMOSTRAS

[0 1 0 ... 1 0 0]
[0 1 0 ... 1 0 0]
