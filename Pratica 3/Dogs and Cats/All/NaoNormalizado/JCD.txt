
#######Dados#############

[[0.  0.  3.  ... 0.  0.  0. ]
 [0.  4.  4.5 ... 0.  0.  0. ]
 [1.  3.5 2.5 ... 0.  0.  0. ]
 ...
 [0.  2.  2.  ... 0.  0.  0. ]
 [1.  4.5 0.5 ... 0.  0.  0. ]
 [6.  5.  2.  ... 0.  0.  0. ]]
Elbow Method: Número de Cluster - 2 Valor - 1317811.1899540348

Elbow Method: Número de Cluster - 3 Valor - 1219860.7431710404

Elbow Method: Número de Cluster - 4 Valor - 1148689.2075166262

Elbow Method: Número de Cluster - 5 Valor - 1096913.3727220104

Elbow Method: Número de Cluster - 6 Valor - 1050924.9852571506

Elbow Method: Número de Cluster - 7 Valor - 1017971.1329459514

Elbow Method: Número de Cluster - 8 Valor - 989388.5153699997

Elbow Method: Número de Cluster - 9 Valor - 964767.636872174

Elbow Method: Número de Cluster - 10 Valor - 941865.5871609021

VARIÂNCIA DAS MELHORES FEATURES
[0.23101829 0.11126038]

COORDENADA DOS CENTROIDES

[[-0.66733148  4.74286294]
 [ 4.2347414  -0.6832574 ]
 [-2.62048914 -0.88422419]]

TABELA DE DISTÂNCIAS

[[ 7.76302133  5.77678162 11.02923034]
 [ 6.06730612  2.66765908  4.20055016]
 [ 4.47590743  3.41130909  3.9639161 ]
 ...
 [11.81864312  5.63483173 12.4929454 ]
 [ 6.22750772  3.23257642  3.70043184]
 [ 8.95333011 10.23455151  3.63290205]]

RÓTULO DADO PELO KMEANS DAS AMOSTRAS

[1 1 1 ... 1 1 2]
[1 1 1 ... 1 1 2]
