
#######Dados#############

[[0. 0. 4. ... 0. 0. 0.]
 [0. 5. 6. ... 0. 0. 0.]
 [2. 6. 4. ... 0. 0. 0.]
 ...
 [0. 3. 3. ... 0. 0. 0.]
 [2. 7. 1. ... 0. 0. 0.]
 [7. 7. 3. ... 0. 0. 0.]]
Elbow Method: Número de Cluster - 2 Valor - 1193979.892659804

Elbow Method: Número de Cluster - 3 Valor - 1101841.113760563

Elbow Method: Número de Cluster - 4 Valor - 1038188.5318032722

Elbow Method: Número de Cluster - 5 Valor - 985807.0560368218

Elbow Method: Número de Cluster - 6 Valor - 943273.5524165937

Elbow Method: Número de Cluster - 7 Valor - 910499.8673781455

Elbow Method: Número de Cluster - 8 Valor - 879809.4433895194

Elbow Method: Número de Cluster - 9 Valor - 857323.0435785332

Elbow Method: Número de Cluster - 10 Valor - 839744.0087519562

VARIÂNCIA DAS MELHORES FEATURES
[0.21267313 0.12808004]

COORDENADA DOS CENTROIDES

[[ 3.54430774 -0.6177334 ]
 [-2.54705955 -1.28146127]
 [-1.06671158  4.07723352]]

TABELA DE DISTÂNCIAS

[[ 8.66751228 11.71173785  6.60983504]
 [ 6.45902586  3.00345692  3.61264953]
 [ 3.37659332  4.32525355  3.29534526]
 ...
 [ 7.29832471  8.01980345  2.0609992 ]
 [ 4.18134176  5.39970796  2.46829827]
 [ 5.62780952  2.02135465  7.93742035]]

RÓTULO DADO PELO KMEANS DAS AMOSTRAS

[2 1 2 ... 2 2 1]
[2 1 2 ... 2 2 1]
